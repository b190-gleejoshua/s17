console.log("hello World")
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function getUserInfo(){
	let fullName = prompt("What is your name?");
	let currentAge = prompt("How old are you?");
	let currentLocation = prompt("Where do you live?");
	alert("Thank you for your input!");

	console.log("Hello, " + fullName);
	console.log("You are " + currentAge + " years old.")
	console.log("You live in " + currentLocation)
};

getUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function bandList(){
	let band1 = "1. Franco";
	let band2 =	"2. Parokya ni Edgar";
	let band3 =	"3. Creed";
	let band4 =	"4. Linkin Park";
	let band5 =	"5. Disturbed";

	console.log(band1)
	console.log(band2)
	console.log(band3)
	console.log(band4)
	console.log(band5)

};

bandList();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function movieList(){
	let movieRating1= "60%";
	let favMovie1 = "1. Bohemian Rhapsody";
	let movieRating2= "89%";
	let favMovie2 = "2. John Wick 4";
	let movieRating3= "93%";
	let favMovie3 = "3. Spiderman No way home";
	let movieRating4= "74%";
	let favMovie4 = "4. Doctor Strange in the Multiverse of madness";
	let movieRating5= "94%";
	let favMovie5 = "5. Avengers Endgame";

	console.log(favMovie1);
	console.log("Rotten Tomatoes rating: " + movieRating1);
	console.log(favMovie2);
	console.log("Rotten Tomatoes rating: " + movieRating2);
	console.log(favMovie3);
	console.log("Rotten Tomatoes rating: " + movieRating3);
	console.log(favMovie4);
	console.log("Rotten Tomatoes rating: " + movieRating4);
	console.log(favMovie5);
	console.log("Rotten Tomatoes rating: " + movieRating5);
	

};
movieList();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


