console.log("hello!")
/*
	FUNCTIONS:
		functions in jvavascript are line/blocks of codes that tell our device/application to perform a certain task when called/invoke
*/


// function declaration
/*
	function - defines a function in javascript; indicator that we are creating a function that will be invoked later in our codes

	printName() - functionName; functions are named to be able to use later in our code
	function block{} - the statements inside the curly brace which comprise of the body of the function . this is where the codes are to be executed


	default syntax:
		function functionName(){
			function statements/code block;
		};
*/

function printName(){
	console.log("My name is John");
};


// call/invoke the function
/*
	the code black and statements inside a function is not immediately executed when the function is defined/declared. the code blocks inside a function will be executed when the function is called /invoked


	it is common to use the term "call a function" instead of "invoke a function"
*/
printName();


// DECLARATION vs EXPRESSION

// results in an error due to the non-existence of the function (not declared)
declaredFunction();

function declaredFunction(){
	console.log("Hello from declaredFunction");
};


// FUNCTION EXPRESSION	
/*
	a function can also be created by storing it inside a variable

	does not allow hoisting

	a function expression is an anonymous function that is assigned to the variableFunction
		anonymous function - unnamed function
*/
// variableFunction(); - calling a function that is created through an expression before it can be defined will result in an error since technically it is a variable

let variableFunction = function(){
	console.log("Hello again!");
};


variableFunction();

/*
	miniactivity
		create two functions
			1st funaction
				log in the console the top three anime you would like to recommend
			2nd function
				log in console the top 3 movies you would like to recommend



*/

function animeFunction(){
	console.log("recommended anime: One Piece, Diamond no Ace, Spy x family");
};

animeFunction()

function movieFunction(){
	console.log("recommended movie: 50 first dates, The longest yard, Grown Ups");
};


movieFunction()

// Reassigning declared functions
// we can also reassign declared functions and functionexpresion to a new anonymous function

declaredFunction = function(){
	console.log("updated declaredFunction");
};

declaredFunction();

// However, we cannot change the declared function/s expressions that are defined using constants
const constFunction= function(){
console.log("Initialized const function")
};
constFunction();


/*constFunction = function(){
console.log("cannot be re-assigned");
};

constFunction();
*/

// Function scoping

/*
	scope is the accessibilit of variables/functions

	Javascript variables/functions have 3 scopes
		1. local/block scope - can be accessed inside the curly brace/code block{}

		2. global scope - anything that is written in the .js file
*/


{
	let localVar = "Armando Perez";
	console.log(localVar);
};

let globalVar = "Mr. Worldwide"

console.log(globalVar);
// console.log(localVar);


/*
	function scope
		JS has also function scope: each function creates a new scope
		variables defined inside a function can only be accessed and used inside that function

*/

function showNames(){
	// Function-scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();

// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

/*functionVar, functionConst, and functionLet are function-scoped and cannot be accessed outside the function they are declared*/


// Nested Functions
/*
	function that are defined inside another function. these nested function have function scope where they can onl be accessed inside the function where they are declared/defined
*/
function newFunction() {
	let name ="Jane"

	function nestedFunction(){
		let nestedName = "John";
		console.log(name); /*valid since we are acessing inside a nested function that is still inside the function where "name" is defined*/
		console.log(nestedName);
	};
	console.log(name);
	nestedFunction()
};

newFunction();
// nestedFunction(); - returns an error since we must call the nestedFunction inside the function where it is declared


let globalName = "Mr. Global";

function sepFunction(){
	let skillSet ="Freediving";


	
	console.log(globalName);
	console.log(sepFunction);
};

sepFunction();


// Using alert()
	// alert()-allows us to show a small window at the top of our browser page to show iformation to our users, as opposed to a conlsole.log() which only shows the message console. It allows us to show a short dialog  or instructions to our user. the page will wait(continue to load) until the user dismisses the dialog


// alert("Hellow World!");


// we can use an alert() to show a message to the user from later function invocation
// function showSampleAlert(){
// 	alert("Hello Again");
// };

// showSampleAlert();
// console.log("I will be displayed after the alert has been closed")

/*
	NOTES ON USING alert()
		show only an alert() for short dialogs/messages to the user
		do not overuser alert() because the program/vs has to wat for it to be dismissed before continuing
*/

// Using prompt()
	// prompt() - used to allow us to show a small window at the top of the browser to gather information
	// like alert(), it will have the page wait until the user completes or enters the input. The input from the prompt() will be returned as a string data type once the user dismisses the window

	/*
		Syntax:
			prompt("<dialogString>")

// 	*/

// let samplePrompt = prompt("Enter your name.")

// console.log("Hello " + samplePrompt)



// let nullPrompt = prompt("do not enter anything here");

// console.log(nullPrompt)


/*

	miniactivity 
		create a function named welcomeMessage
			create 2 variables
				the firstName of the user that will come from a prompt
				the firstName of the user that will come from a prompt

			log in the console the message "Hello firstName lastName"

		invoke the function
*/

// function welcomeMessage(){
// 	let firstName =prompt("Please enter first Name");
// 	let lastName = prompt("Please enter Last Name");

// 	console.log("Hello " + firstName+" " + lastName);

// };

// welcomeMessage()

// Function Naming

function getCourses(){
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}

getCourses();

function get(){
	let name = "Jamie";
	console.log(name);
};

get();


function foo(){
	console.log(25%5);
};

foo();